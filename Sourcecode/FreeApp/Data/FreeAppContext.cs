using FreeApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace FreeApp.Data
{
    public class FreeAppContext : IdentityDbContext<UserMember>
    {
        public DbSet<TypeApp> TypeApp {get; set;}
        public DbSet<Apps> App {get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=FreeApp.db");
        }

    }
} 