using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;

 
namespace FreeApp.Models
{
    public class UserMember : IdentityUser
    {
        public string FirstName {get; set;}
        public string LastName {get; set;}
    }

    public class TypeApp 
    {
        public int TypeAppID {get; set;}
        public string TypeName {get; set;}
    }

    public class Apps 
    {
        public int AppsId{get; set;}
        public string AppName {get; set;}

        public int TypeAppID {get; set;}
        public TypeApp Type {get; set;}

        public string UserMemberId {get; set;}
        public UserMember byUser {get; set;} 
        
        [DataType(DataType.Date)]
        public string InputDate {get; set;}
        public string AppDetail {get; set;}
        public string AppLink {get; set;}
        public string AppDev {get; set;}
        public string AppImage {get; set;}
    }
    
}