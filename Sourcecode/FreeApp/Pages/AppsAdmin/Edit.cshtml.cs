using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FreeApp.Data;
using FreeApp.Models;
using Microsoft.AspNetCore.Http;

namespace FreeApp.Pages.AppsAdmin
{
    public class EditModel : PageModel
    {
        private readonly FreeApp.Data.FreeAppContext _context;

        public EditModel(FreeApp.Data.FreeAppContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Apps Apps { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Apps = await _context.App
                .Include(a => a.Type)
                .Include(a => a.byUser).FirstOrDefaultAsync(m => m.AppsId == id);

            if (Apps == null)
            {
                return NotFound();
            }
           ViewData["TypeAppID"] = new SelectList(_context.TypeApp, "TypeAppID", "TypeName");
           ViewData["UserMemberId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        
        public async Task<IActionResult> OnPostAsync(IFormFile fileUpload,int? id) // เรียกใช้ Id เพื่อสร้างobject temp
        {
            if (!ModelState.IsValid)
            {
                TempData.Keep("AppImage");
                return Page();
            }

            _context.Attach(Apps); // แยกส่วนจากของเดิม _context.Attach(Apps).State = EntityState.Modified; 

            Apps temp = new FreeAppContext().App.Find(Apps.AppsId); //เก็บค่าApps อันเก่า

            Apps.AppImage = temp.AppImage;

            temp = null; //ลบข้อมูลทิ้ง
            
            if (fileUpload != null) //เช็คว่ามีการแก้ไขรูปภาพไหม
            {
                string pathImgApp = "/images/App/";
                string pathSave = $"wwwroot{pathImgApp}";
                if(!Directory.Exists(pathSave)) 
                {
                    Directory.CreateDirectory(pathSave);
                }
                string fileName = fileUpload.FileName;
                var path = Path.Combine(Directory.GetCurrentDirectory(),pathSave,fileName);
            
                using (var stream = new FileStream(path,FileMode.Create))
                {
                    await fileUpload.CopyToAsync(stream);
                }

                Apps.AppImage = pathImgApp+fileName;
            }
            
            _context.Entry(Apps).State = EntityState.Modified; 

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AppsExists(Apps.AppsId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool AppsExists(int id)
        {
            return _context.App.Any(e => e.AppsId == id);
        }
    }
}
