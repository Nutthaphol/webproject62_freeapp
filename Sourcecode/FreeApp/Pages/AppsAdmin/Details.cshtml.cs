using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FreeApp.Data;
using FreeApp.Models;

namespace FreeApp.Pages.AppsAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly FreeApp.Data.FreeAppContext _context;

        public DetailsModel(FreeApp.Data.FreeAppContext context)
        {
            _context = context;
        }

        public Apps Apps { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Apps = await _context.App
                .Include(a => a.Type)
                .Include(a => a.byUser).FirstOrDefaultAsync(m => m.AppsId == id);

            if (Apps == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
