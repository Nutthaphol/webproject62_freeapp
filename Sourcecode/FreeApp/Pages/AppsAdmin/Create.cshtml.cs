using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using FreeApp.Data;
using FreeApp.Models;
using Microsoft.AspNetCore.Http;

namespace FreeApp.Pages.AppsAdmin
{
    public class CreateModel : PageModel
    {
        private readonly FreeApp.Data.FreeAppContext _context;

        public CreateModel(FreeApp.Data.FreeAppContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["TypeAppID"] = new SelectList(_context.TypeApp, "TypeAppID", "TypeName");
        ViewData["UserMemberId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public Apps Apps { get; set; }
        

        public async Task<IActionResult> OnPostAsync(IFormFile fileUpload)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            if (fileUpload != null)
            {
                string pathImgApp = "/images/App/";
                string pathSave = $"wwwroot{pathImgApp}";
                string fileName = fileUpload.FileName;
                var path = Path.Combine(Directory.GetCurrentDirectory(),pathSave,fileName);
            
                using (var stream = new FileStream(path,FileMode.Create))
                {
                    await fileUpload.CopyToAsync(stream);
                }

                Apps.AppImage = pathImgApp+fileName;
            }   
            _context.App.Add(Apps);
            await _context.SaveChangesAsync();
            return RedirectToPage("./Index");
            
        }
    }
}