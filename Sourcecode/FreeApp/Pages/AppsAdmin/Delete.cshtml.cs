using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FreeApp.Data;
using FreeApp.Models;

namespace FreeApp.Pages.AppsAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly FreeApp.Data.FreeAppContext _context;

        public DeleteModel(FreeApp.Data.FreeAppContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Apps Apps { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Apps = await _context.App
                .Include(a => a.Type)
                .Include(a => a.byUser).FirstOrDefaultAsync(m => m.AppsId == id);

            if (Apps == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Apps = await _context.App.FindAsync(id);

            if (Apps != null)
            {
                _context.App.Remove(Apps);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
