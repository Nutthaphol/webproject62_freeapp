using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FreeApp.Data;
using FreeApp.Models;

namespace FreeApp.Pages.AppsAdmin
{
    public class IndexModel : PageModel
    {
        private readonly FreeApp.Data.FreeAppContext _context;

        public IndexModel(FreeApp.Data.FreeAppContext context)
        {
            _context = context;
        }

        public IList<Apps> Apps { get;set; } 

        public async Task OnGetAsync()
        {
            Apps = await _context.App
                .Include(a => a.Type)
                .Include(b => b.byUser).ToListAsync();
        }
    }
}
