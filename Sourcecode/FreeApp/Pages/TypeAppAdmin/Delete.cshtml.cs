using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FreeApp.Data;
using FreeApp.Models;

namespace FreeApp.Pages.TypeAppAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly FreeApp.Data.FreeAppContext _context;

        public DeleteModel(FreeApp.Data.FreeAppContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TypeApp TypeApp { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TypeApp = await _context.TypeApp.FirstOrDefaultAsync(m => m.TypeAppID == id);

            if (TypeApp == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TypeApp = await _context.TypeApp.FindAsync(id);

            if (TypeApp != null)
            {
                _context.TypeApp.Remove(TypeApp);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
