using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FreeApp.Data;
using FreeApp.Models;

namespace FreeApp.Pages.TypeAppAdmin
{
    public class IndexModel : PageModel
    {
        private readonly FreeApp.Data.FreeAppContext _context;

        public IndexModel(FreeApp.Data.FreeAppContext context)
        {
            _context = context;
        }

        public IList<TypeApp> TypeApp { get;set; }

        public async Task OnGetAsync()
        {
            TypeApp = await _context.TypeApp.ToListAsync();
        }
    }
}
