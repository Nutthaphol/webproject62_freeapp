using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using FreeApp.Data;
using FreeApp.Models;

namespace FreeApp.Pages.TypeAppAdmin
{
    public class CreateModel : PageModel
    {
        private readonly FreeApp.Data.FreeAppContext _context;

        public CreateModel(FreeApp.Data.FreeAppContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public TypeApp TypeApp { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            

            _context.TypeApp.Add(TypeApp);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}