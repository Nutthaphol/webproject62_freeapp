using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FreeApp.Data;
using FreeApp.Models;

namespace FreeApp.Pages.TypeAppAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly FreeApp.Data.FreeAppContext _context;

        public DetailsModel(FreeApp.Data.FreeAppContext context)
        {
            _context = context;
        }

        public TypeApp TypeApp { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TypeApp = await _context.TypeApp.FirstOrDefaultAsync(m => m.TypeAppID == id);

            if (TypeApp == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
