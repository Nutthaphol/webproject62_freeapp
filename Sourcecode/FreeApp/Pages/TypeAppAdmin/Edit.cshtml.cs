using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FreeApp.Data;
using FreeApp.Models;

namespace FreeApp.Pages.TypeAppAdmin
{
    public class EditModel : PageModel
    {
        private readonly FreeApp.Data.FreeAppContext _context;

        public EditModel(FreeApp.Data.FreeAppContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TypeApp TypeApp { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TypeApp = await _context.TypeApp.FirstOrDefaultAsync(m => m.TypeAppID == id);

            if (TypeApp == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(TypeApp).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TypeAppExists(TypeApp.TypeAppID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool TypeAppExists(int id)
        {
            return _context.TypeApp.Any(e => e.TypeAppID == id);
        }
    }
}
