﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.EntityFrameworkCore;
using FreeApp.Data;
using FreeApp.Models;

namespace FreeApp.Pages
{
    public class IndexModel : PageModel
    {   
        private readonly FreeApp.Data.FreeAppContext _context;

        public IndexModel(FreeApp.Data.FreeAppContext context)
        {
            _context = context;
        }
        
        public IList<Apps> Apps { get;set; }
        public IList<TypeApp> TypeApp { get;set; }
        public Apps ran;
    
        public async Task OnGetAsync()
        {
            Apps = await _context.App
                .Include(t => t.Type)
                .Include(b => b.byUser).ToListAsync();

            TypeApp = await _context.TypeApp.ToListAsync();

            ran = Apps[new Random().Next(Apps.Count)]; 
        }    
    }
}
