#pragma checksum "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "06c686c2d7839c8aebe061760acd66470835464b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(FreeApp.Pages.AppsAdmin.Pages_AppsAdmin_Details), @"mvc.1.0.razor-page", @"/Pages/AppsAdmin/Details.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/AppsAdmin/Details.cshtml", typeof(FreeApp.Pages.AppsAdmin.Pages_AppsAdmin_Details), null)]
namespace FreeApp.Pages.AppsAdmin
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\_ViewImports.cshtml"
using FreeApp;

#line default
#line hidden
#line 2 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 3 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml"
using FreeApp.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"06c686c2d7839c8aebe061760acd66470835464b", @"/Pages/AppsAdmin/Details.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5279af0b0e55a7f7122287c6ca3005415697bcc8", @"/Pages/_ViewImports.cshtml")]
    public class Pages_AppsAdmin_Details : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("height", new global::Microsoft.AspNetCore.Html.HtmlString("60px"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("width", new global::Microsoft.AspNetCore.Html.HtmlString("60px"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "./Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "./Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.ImageTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(68, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(209, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 9 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml"
  
    ViewData["Title"] = "รายละเอียดโปรแกรม";

#line default
#line hidden
            BeginContext(264, 215, true);
            WriteLiteral("\r\n<h2>รายละเอียดโปรแกรม</h2>\r\n\r\n<div>\r\n    <hr />\r\n    <dl class=\"dl-horizontal\">\r\n        <dt>\r\n            <h4 id = \"index-app\">ประเภทของโปรแกรม</h4>\r\n        </dt>\r\n        <dd>\r\n            <h4 id = \"index-app\">");
            EndContext();
            BeginContext(480, 50, false);
#line 22 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml"
                            Write(Html.DisplayFor(model => model.Apps.Type.TypeName));

#line default
#line hidden
            EndContext();
            BeginContext(530, 149, true);
            WriteLiteral("</h4>\r\n        </dd>\r\n        <dt>\r\n            <h4 id = \"index-app\">ชื่อโปรแกรม</h4>\r\n        </dt>\r\n        <dd>\r\n            <h4 id = \"index-app\">");
            EndContext();
            BeginContext(680, 44, false);
#line 28 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml"
                            Write(Html.DisplayFor(model => model.Apps.AppName));

#line default
#line hidden
            EndContext();
            BeginContext(724, 145, true);
            WriteLiteral("</h4>\r\n        </dd>\r\n        <dt>\r\n            <h4 id = \"index-app\">ผู้แชร์</h4>\r\n        </dt>\r\n        <dd>\r\n            <h4 id = \"index-app\">");
            EndContext();
            BeginContext(870, 53, false);
#line 34 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml"
                            Write(Html.DisplayFor(model => model.Apps.byUser.FirstName));

#line default
#line hidden
            EndContext();
            BeginContext(923, 157, true);
            WriteLiteral("</h4>\r\n        </dd>\r\n        <dt>\r\n            <h4 id = \"index-app\">วันที่แชร์/ปรับปรุง</h4>\r\n        </dt>\r\n        <dd>\r\n            <h4 id = \"index-app\">");
            EndContext();
            BeginContext(1081, 46, false);
#line 40 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml"
                            Write(Html.DisplayFor(model => model.Apps.InputDate));

#line default
#line hidden
            EndContext();
            BeginContext(1127, 154, true);
            WriteLiteral("</h4>\r\n        </dd>\r\n        <dt>\r\n            <h4 id =\"index-app\">รายละเอียดโปรแกรม</h4>\r\n        </dt>\r\n        <dd>\r\n            <h4 id = \"index-app\">");
            EndContext();
            BeginContext(1282, 46, false);
#line 46 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml"
                            Write(Html.DisplayFor(model => model.Apps.AppDetail));

#line default
#line hidden
            EndContext();
            BeginContext(1328, 158, true);
            WriteLiteral("</h4>\r\n        </dd>\r\n        <dt>\r\n            <h4 id = \"index-app\">ลิงค์สำหรับดวาน์โหลด</h4>\r\n        </dt>\r\n        <dd>\r\n            <h4 id = \"index-app\">");
            EndContext();
            BeginContext(1487, 44, false);
#line 52 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml"
                            Write(Html.DisplayFor(model => model.Apps.AppLink));

#line default
#line hidden
            EndContext();
            BeginContext(1531, 153, true);
            WriteLiteral("</h4>\r\n        </dd>\r\n        <dt>\r\n            <h4 id = \"index-app\">ผู้พัฒนาโปรแกรม</h4>\r\n        </dt>\r\n        <dd>\r\n            <h4 id = \"index-app\">");
            EndContext();
            BeginContext(1685, 43, false);
#line 58 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml"
                            Write(Html.DisplayFor(model => model.Apps.AppDev));

#line default
#line hidden
            EndContext();
            BeginContext(1728, 121, true);
            WriteLiteral("</h4>\r\n        </dd>\r\n        <dt>\r\n            <h4 id = \"index-app\">Icon</h4>\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1849, 86, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "9c44b018791d4fdfaea4d254d074b748", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.ImageTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper);
            BeginWriteTagHelperAttribute();
            WriteLiteral("~");
#line 64 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml"
           WriteLiteral(Model.Apps.AppImage);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper.Src = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("src", __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper.Src, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#line 64 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper.AppendVersion = true;

#line default
#line hidden
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-append-version", __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper.AppendVersion, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1935, 47, true);
            WriteLiteral("\r\n        </dd>\r\n    </dl>\r\n</div>\r\n<div>\r\n    ");
            EndContext();
            BeginContext(1982, 134, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b5621ad8875b46a78d87e935b0acd4e6", async() => {
                BeginContext(2037, 75, true);
                WriteLiteral("<button style = \"background-color:#6666cc;color:#ffffff\">ปรับปรุง </button>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 69 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Details.cshtml"
                           WriteLiteral(Model.Apps.AppsId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2116, 8, true);
            WriteLiteral(" |\r\n    ");
            EndContext();
            BeginContext(2124, 40, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "89dd4ab343444508a4b9952879ac41b5", async() => {
                BeginContext(2146, 14, true);
                WriteLiteral("กลับไปหน้าแชร์");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2164, 10, true);
            WriteLiteral("\r\n</div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public UserManager<UserMember> UserManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public SignInManager<UserMember> SignInManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<FreeApp.Pages.AppsAdmin.DetailsModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<FreeApp.Pages.AppsAdmin.DetailsModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<FreeApp.Pages.AppsAdmin.DetailsModel>)PageContext?.ViewData;
        public FreeApp.Pages.AppsAdmin.DetailsModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
