#pragma checksum "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b086cc6cf3931010f078a51b98fca94718a6ed44"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(FreeApp.Pages.AppsAdmin.Pages_AppsAdmin_Index), @"mvc.1.0.razor-page", @"/Pages/AppsAdmin/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/AppsAdmin/Index.cshtml", typeof(FreeApp.Pages.AppsAdmin.Pages_AppsAdmin_Index), null)]
namespace FreeApp.Pages.AppsAdmin
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\_ViewImports.cshtml"
using FreeApp;

#line default
#line hidden
#line 2 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 3 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
using FreeApp.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b086cc6cf3931010f078a51b98fca94718a6ed44", @"/Pages/AppsAdmin/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5279af0b0e55a7f7122287c6ca3005415697bcc8", @"/Pages/_ViewImports.cshtml")]
    public class Pages_AppsAdmin_Index : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "Create", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("height", new global::Microsoft.AspNetCore.Html.HtmlString("60px"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("width", new global::Microsoft.AspNetCore.Html.HtmlString("60px"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "./Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "./Details", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "./Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.ImageTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(68, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(207, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 9 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
  
    ViewData["Title"] = "แชร์";

#line default
#line hidden
            BeginContext(249, 30, true);
            WriteLiteral("\r\n<h2>Share</h2>\r\n\r\n<h4>\r\n    ");
            EndContext();
            BeginContext(279, 98, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "32282b9e5f324c989d6156e39bea0135", async() => {
                BeginContext(300, 73, true);
                WriteLiteral("<button style = \"background-color:#6666cc;color:#ffffff\">แบ่งปัน</button>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(377, 838, true);
            WriteLiteral(@"
</h4>
<table class=""table"">
    <thead>
        <tr>
            <th>
                <p id = ""index-app"">ประเภทโปรแกรม</p>
            </th>
            <th>
                <p id = ""index-app"">ชื่อโปรแกรม</p>
            </th>
            <th>
                <p id = ""index-app"">Icon</p>
            </th>
            <th>
                <p id = ""index-app"">ผู้แชร์</p>
            </th>
            <th>
                <p id = ""index-app"">วันที่แชร์</p>
            </th>
            <th>
                <p id = ""index-app"">คำอธิบาย</p>
            </th>
            <th>
                <p id = ""index-app"">ลิงค์ดาวน์โหลด</p>
            </th>
            <th>
                <p id = ""index-app"">ผู้พัฒนาโปรแกรม</p>
            </th>
            <th></th>
        </tr>
    </thead>
    <tbody>
");
            EndContext();
#line 49 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
 foreach (var item in Model.Apps) {

#line default
#line hidden
            BeginContext(1252, 48, true);
            WriteLiteral("        <tr>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(1301, 48, false);
#line 52 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Type.TypeName));

#line default
#line hidden
            EndContext();
            BeginContext(1349, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(1405, 42, false);
#line 55 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.AppName));

#line default
#line hidden
            EndContext();
            BeginContext(1447, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(1502, 80, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "8ea44704e66f4b81996497b35f6a8ca1", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.ImageTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper);
            BeginWriteTagHelperAttribute();
            WriteLiteral("~");
#line 58 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
               WriteLiteral(item.AppImage);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper.Src = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("src", __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper.Src, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#line 58 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper.AppendVersion = true;

#line default
#line hidden
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-append-version", __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper.AppendVersion, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1582, 56, true);
            WriteLiteral(" \r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(1639, 51, false);
#line 61 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.byUser.FirstName));

#line default
#line hidden
            EndContext();
            BeginContext(1690, 2, true);
            WriteLiteral("  ");
            EndContext();
            BeginContext(1693, 50, false);
#line 61 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
                                                                 Write(Html.DisplayFor(modelItem => item.byUser.LastName));

#line default
#line hidden
            EndContext();
            BeginContext(1743, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(1799, 44, false);
#line 64 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.InputDate));

#line default
#line hidden
            EndContext();
            BeginContext(1843, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(1899, 44, false);
#line 67 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.AppDetail));

#line default
#line hidden
            EndContext();
            BeginContext(1943, 57, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                <a");
            EndContext();
            BeginWriteAttribute("href", " href =\"", 2000, "\"", 2051, 1);
#line 70 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
WriteAttributeValue("", 2008, Html.DisplayFor(modelItem => item.AppLink), 2008, 43, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2052, 18, true);
            WriteLiteral(" target=\"_blank\" >");
            EndContext();
            BeginContext(2071, 42, false);
#line 70 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
                                                                                   Write(Html.DisplayFor(modelItem => item.AppLink));

#line default
#line hidden
            EndContext();
            BeginContext(2113, 59, true);
            WriteLiteral("</a>\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(2173, 41, false);
#line 73 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.AppDev));

#line default
#line hidden
            EndContext();
            BeginContext(2214, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(2269, 124, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e78712f4c97f4044a20d61758f17c93d", async() => {
                BeginContext(2318, 71, true);
                WriteLiteral("<button style = \"background-color:#6666cc;color:#ffffff\" >Edit</button>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 76 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
                                       WriteLiteral(item.AppsId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2393, 19, true);
            WriteLiteral(" \r\n                ");
            EndContext();
            BeginContext(2412, 129, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "941d7e78961f432f8f3032866040c2af", async() => {
                BeginContext(2464, 73, true);
                WriteLiteral("<button style = \"background-color:#6666cc;color:#ffffff\" >Detail</button>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 77 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
                                          WriteLiteral(item.AppsId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2541, 19, true);
            WriteLiteral(" \r\n                ");
            EndContext();
            BeginContext(2560, 128, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b7592636d9434f73aec916d06442bf50", async() => {
                BeginContext(2611, 73, true);
                WriteLiteral("<button style = \"background-color:#ff5555;color:#ffffff\" >Delete</button>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 78 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
                                         WriteLiteral(item.AppsId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2688, 36, true);
            WriteLiteral("\r\n            </td>\r\n        </tr>\r\n");
            EndContext();
#line 81 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\AppsAdmin\Index.cshtml"
}

#line default
#line hidden
            BeginContext(2727, 24, true);
            WriteLiteral("    </tbody>\r\n</table>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public UserManager<UserMember> UserManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public SignInManager<UserMember> SignInManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<FreeApp.Pages.AppsAdmin.IndexModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<FreeApp.Pages.AppsAdmin.IndexModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<FreeApp.Pages.AppsAdmin.IndexModel>)PageContext?.ViewData;
        public FreeApp.Pages.AppsAdmin.IndexModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
