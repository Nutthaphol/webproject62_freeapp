#pragma checksum "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c986e6f2a3b7b963dd77ef20bf11ca7c0803a0b9"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(FreeApp.Pages.Pages_Index), @"mvc.1.0.razor-page", @"/Pages/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/Index.cshtml", typeof(FreeApp.Pages.Pages_Index), null)]
namespace FreeApp.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\_ViewImports.cshtml"
using FreeApp;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c986e6f2a3b7b963dd77ef20bf11ca7c0803a0b9", @"/Pages/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5279af0b0e55a7f7122287c6ca3005415697bcc8", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Index : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("height", new global::Microsoft.AspNetCore.Html.HtmlString("100px"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("width", new global::Microsoft.AspNetCore.Html.HtmlString("100px"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.ImageTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
  
    ViewData["Title"] = "Home page";

#line default
#line hidden
            BeginContext(71, 236, true);
            WriteLiteral("\r\n<div class = \"jumbotron\" style = \"background-color:#6666cc; color:#ffffff\">\r\n    <h1 id = \"HeadJum\">Welcome to FreeApp</h1>\r\n    <p id = \"HeadJum\">Websites for application sharing That can be used for free and licensed</p>\r\n</div>\r\n\r\n");
            EndContext();
#line 12 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
 foreach (var type in Model.TypeApp){

#line default
#line hidden
            BeginContext(346, 45, true);
            WriteLiteral("    <h1 style = \"text-decoration: underline\">");
            EndContext();
            BeginContext(392, 43, false);
#line 13 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
                                        Write(Html.DisplayFor(modelItem => type.TypeName));

#line default
#line hidden
            EndContext();
            BeginContext(435, 7, true);
            WriteLiteral("</h1>\r\n");
            EndContext();
#line 14 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
     foreach (var app in Model.Apps){
    if (@app.TypeAppID == type.TypeAppID){

#line default
#line hidden
            BeginContext(525, 86, true);
            WriteLiteral("    <div class = \"row\">\r\n        <div class = \"col-md-2\">\r\n        <br> \r\n            ");
            EndContext();
            BeginContext(611, 81, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "2cd7de6baf5a4e239f810da1601dec93", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.ImageTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper);
            BeginWriteTagHelperAttribute();
            WriteLiteral("~");
#line 19 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
           WriteLiteral(app.AppImage);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper.Src = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("src", __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper.Src, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#line 19 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper.AppendVersion = true;

#line default
#line hidden
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-append-version", __Microsoft_AspNetCore_Mvc_TagHelpers_ImageTagHelper.AppendVersion, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(692, 97, true);
            WriteLiteral(" \r\n        </div>\r\n\r\n        <div class = \"col-md-10\" id =\"data\">\r\n            <h3 id =\"appName\">");
            EndContext();
            BeginContext(790, 11, false);
#line 23 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
                         Write(app.AppName);

#line default
#line hidden
            EndContext();
            BeginContext(801, 51, true);
            WriteLiteral("</h3> \r\n            <b>รายละเอียด</b>      ->      ");
            EndContext();
            BeginContext(853, 43, false);
#line 24 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
                                      Write(Html.DisplayFor(modelItem => app.AppDetail));

#line default
#line hidden
            EndContext();
            BeginContext(896, 50, true);
            WriteLiteral("<br>\r\n            <b>ผู้ฟัฒนา</b>         ->      ");
            EndContext();
            BeginContext(947, 40, false);
#line 25 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => app.AppDev));

#line default
#line hidden
            EndContext();
            BeginContext(987, 59, true);
            WriteLiteral("<br>   \r\n            <b>ลิงก์ดาวน์โหลด</b>       ->      <a");
            EndContext();
            BeginWriteAttribute("href", " href = \"", 1046, "\"", 1097, 1);
#line 26 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
WriteAttributeValue("", 1055, Html.DisplayFor(modelItem => app.AppLink), 1055, 42, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1098, 17, true);
            WriteLiteral(" target=\"_blank\">");
            EndContext();
            BeginContext(1116, 41, false);
#line 26 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
                                                                                                                  Write(Html.DisplayFor(modelItem => app.AppLink));

#line default
#line hidden
            EndContext();
            BeginContext(1157, 54, true);
            WriteLiteral("</a><br>\r\n            <b>โพสต์โดย</b>         ->      ");
            EndContext();
            BeginContext(1212, 50, false);
#line 27 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => app.byUser.FirstName));

#line default
#line hidden
            EndContext();
            BeginContext(1262, 1, true);
            WriteLiteral(" ");
            EndContext();
            BeginContext(1264, 49, false);
#line 27 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
                                                                                           Write(Html.DisplayFor(modelItem => app.byUser.LastName));

#line default
#line hidden
            EndContext();
            BeginContext(1313, 41, true);
            WriteLiteral("<br>   \r\n        </div>\r\n    </div><br>\r\n");
            EndContext();
#line 30 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
    }
    }

#line default
#line hidden
#line 31 "D:\6110210129MiniProject\webproject62_freeapp\Sourcecode\FreeApp\Pages\Index.cshtml"
     
}

#line default
#line hidden
            BeginContext(1371, 2, true);
            WriteLiteral("\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel>)PageContext?.ViewData;
        public IndexModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
